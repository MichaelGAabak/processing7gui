// array functions
// store and read positions, safe and read to/from file
//

void pos_record(){ // ------------------------------------------ save target pos in array / command 'space'
    if (step < stepMax){ // insert data > move all entrys 1 step up
      for (int i = stepMax; i > step; i--){
        for (int j = 0; j < 20; j++){ 
          APtgt[i+1][j] = APtgt[i][j];
        }
      } 
    }
    stepMax += 1;
    step += 1; // add data    
    for (int i=0; i < SERVO_NUM; i++){
      APtgt[step][i] = posD[i];
    }  
    APtgt[step][19] = KmodeFlag;
    if (!gripper) // if not gripper value is get from pump
      APtgt[step][6] = Vpump;
    APtgt[step][7] = Xtgt; APtgt[step][8] = Ytgt; APtgt[step][9] = Ztgt;
    APtgt[step][10] = (int)vec56.x; APtgt[step][11] = (int)vec56.y; APtgt[step][12] = (int)vec56.z;
    APtgt[step][13] = (int)vec67.x; APtgt[step][14] = (int)vec67.y; APtgt[step][15] = (int)vec67.z;
    APtgt[step][16] = APtgt[step][6];

    //if(DEBUG)
    {
      print("add step " + step + " > " + APtgt[step][19] + " ");
      for (int i=0; i < 19; i++){
        print(APtgt[step][i] + " ");}
      println(" ");}
}

void pos_clear(){ // ------------------------------------------------------------ Clear last step 'C'  
  if (step < stepMax){
    for (int i = step; i < stepMax; i++){
      for (int j = 0; j < 20; j++){ 
        APtgt[i][j] = APtgt[i+1][j];
      }
    }
  }
  else
    step -= 1;
  print("clear pos "); println(step); // DEBUG print out
  //step = -1;
  stepMax = stepMax-1;  
}

void read_Array(){ // read array  / command 'P'
  for (int i=0; i < 20; i++){ // get servo pos
    Ptgt[i] = APtgt[step][i];
  }
  Xtgt = APtgt[step][7]; // get IK coordinates
  Ytgt = APtgt[step][8];
  Ztgt = APtgt[step][9];
  KmodeFlag = APtgt[step][19];
  for (int i=0; i < SERVO_NUM; i++){ // make angles from target
    angles[i] = Ptgt[i]*9/50;
  }
  if(DEBUG){
  print(step + " > " + APtgt[step][19] + " ");
    for (int i=0; i < 19; i++){ // make angles from target
      print(APtgt[step][i] + " ");}
    println(" ");}
}

void safe_array(){ // ------------------------------------------------------ safe to disk command 'S'
  
  TableRow newRow = table.addRow();
  table.clearRows();
  for (int i=1; i < stepMax+1; i++){ // run true all steps
    newRow = table.addRow();
    newRow.setInt("step", i);
    newRow.setInt("function", APtgt[i][19]);// FK or IK flag
    //print("safe: "+ i);delay(100);
    newRow.setInt("axis0", int(APtgt[i][0]));// axis 0
    newRow.setInt("axis1", APtgt[i][1]); newRow.setInt("axis2", APtgt[i][2]); newRow.setInt("axis3", APtgt[i][3]);
    newRow.setInt("axis4", APtgt[i][4]); newRow.setInt("axis5", APtgt[i][5]); newRow.setInt("axis6", APtgt[i][6]);// axis 7
    newRow.setInt("Xtgt", APtgt[i][7]);//  IK X
    newRow.setInt("Ytgt", APtgt[i][8]); newRow.setInt("Ztgt", APtgt[i][9]); newRow.setInt("56x", APtgt[i][10]);//  IK V56
    newRow.setInt("56y", APtgt[i][11]); newRow.setInt("56z", APtgt[i][12]); newRow.setInt("67x", APtgt[i][13]);//  IK V67
    newRow.setInt("67y", APtgt[i][14]); newRow.setInt("67z", APtgt[i][15]); newRow.setInt("the", APtgt[i][16]);//  IK Theta
  }
  println("save file: " + fileName+".csv");
  saveTable(table, "data/"+fileName+".csv");
}

void read_file(){ //------------------------------ read from file and write in APtgt array / command 'O'
  
  cnt1 = 0;
  table = loadTable(fileName+".csv", "header");
  for (TableRow row : table.rows()) {
    cnt1 += 1; stepMax = cnt1-1; step = 1;
    int id = row.getInt("step");
    APtgt[id][19] = row.getInt("function");
    APtgt[id][0] = row.getInt("axis0"); APtgt[id][1] = row.getInt("axis1");
    APtgt[id][2] = row.getInt("axis2"); APtgt[id][3] = row.getInt("axis3");
    APtgt[id][4] = row.getInt("axis4"); APtgt[id][5] = row.getInt("axis5");
    APtgt[id][6] = row.getInt("axis6"); APtgt[id][7] = row.getInt("Xtgt");
    APtgt[id][8] = row.getInt("Ytgt"); APtgt[id][9] = row.getInt("Ztgt");
    APtgt[id][10] = row.getInt("56x"); APtgt[id][11] = row.getInt("56y");
    APtgt[id][12] = row.getInt("56z"); APtgt[id][13] = row.getInt("67x");
    APtgt[id][14] = row.getInt("67y"); APtgt[id][15] = row.getInt("67z");
    APtgt[id][16] = row.getInt("the");
    print(id + " > " + APtgt[id][19] + " ");
    for (int i=0; i < 19; i++){ // make angles from target
      print(APtgt[id][i] + " ");}
    println(" ");
    step = id; stepMax = step;
}
  println("file: " + fileName+".csv" + " ready...");
}
