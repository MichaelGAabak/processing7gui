// calculating speed for each servo 
// set poses
// 

void calculate_speed(){ // ------------------------------------------------ calculate speed for each servo to get move end sync

  for (int i=0; i < SERVO_NUM; i++){ // find travel distances for each servo
    Perr[i] = abs(Pnow[i] - APtgt[step][i]);
  }
  
  PerrMax = max(Perr[0],Perr[1]); // find largest error
  for (int i=2; i < SERVO_NUM; i++){    
    PerrMax = max(PerrMax,Perr[i]);
  }
  
  float PerrNoun = 0.1*(1000 - PerrMax); // add extra speed to all axis in %
  for (int i=0; i < 6; i++){ // add speed to 250
    Perr[i] += (Perr[i]*0.01)*(int)PerrNoun;
  }  
  
  for (int i=0; i < 6; i++){// gripper servo not includet in speed calculation!
    speed [i] = (int)map((Perr[i])+10,10,1000,10,speedMax);// map distances to speed
  }
  speed [6] = 50; // gripper with fixed speed 
  setSpeed(fluentEnables, speed); // set speed
}

void run_math(){// --------------------------------------------- play computed moves

  a += 10.0; if(a > 360) {a = 0;}// 2 circles and 10s pause
  float rad = radians(a+180);
  Xtgt = int(0+sin(rad)*80);
  Ytgt = int(240+cos(rad)*80);
  Ztgt = int(100);
  theta6 = 75;
  get_pose();  
  j6 = new PVector(Xtgt, Ytgt, Ztgt);

  if(DEBUG){
  print(step + " > " + APtgt[step][19] + " ");
    for (int i=0; i < 19; i++){ // make angles from target
      print(APtgt[step][i] + " ");}
    println(" ");}

}
void get_position(){// --------------------------------------------- get arm position from sliders
    Xtgt = (int)XYslider.getArrayValue()[0]; // IK field input
    Ytgt = (int)XYslider.getArrayValue()[1];
    Ztgt = (int)Zslider.getValue();
    j6 = new PVector(Xtgt, Ytgt, Ztgt);
}
void get_pose(){  // ------------------------------------------------ get hand pose from sliders
   
  X56 = (int)V56slider.getArrayValue()[0]; // relative to arm coordinates for hand orientation
  Y56 = (int)V56slider.getArrayValue()[1];
  Z56 = (int)V56Zslider.getValue();
  if (!focusC){// absulute coordinates for hand orientation
    X56 += Xtgt; Y56 += Ytgt;  Z56 += Ztgt;} 
  vec56 = new PVector(X56-Xtgt, Y56-Ytgt, Z56-Ztgt);
  
  Z67 = (int)V67Zknob.getValue();//V67ZknobV67Zslider
  vec67 = new PVector(X67, Y67, Z67);
  
  theta5 =  90+(Z67/2); // slider to rotation
  theta6 = Vpump*9/50; // gripper or/and pump
  if (gripper) theta6 = (int)Gripper.getValue();
}
