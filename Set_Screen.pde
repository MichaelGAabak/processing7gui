// show the screen
//
//

void Set_Screen(){ // at startup

  background(0,0,40);
  noFill();
  noStroke();
  smooth();

  stroke(250);
  textSize(12);
  text("80's style GUI for 7Bot v0.9.3",603,480);
  img = loadImage("7Bot_Logo.jpg");
}

void update_slider(){ // update axis sliders
  cp5.getController("Joint 0").setValue(posD[0]*9/50);// tell slider the values from axis0-7
  cp5.getController("Joint 1").setValue(posD[1]*9/50);
  cp5.getController("Joint 2").setValue(posD[2]*9/50);
  cp5.getController("Joint 3").setValue(posD[3]*9/50);
  cp5.getController("Joint 4").setValue(posD[4]*9/50);
  cp5.getController("Joint 5").setValue(posD[5]*9/50);
  cp5.getController("Joint 6").setValue(posD[6]*9/50);
  cp5.get(Slider.class, "Pump").setValue(0);
    if (pump) cp5.get(Slider.class, "Pump").setValue(1);
}

void update_IKslider(){ // update IK sliders while play

    cp5.get(Slider2D.class, "Arm [XY]").setValue(Xtgt,Ytgt);
    cp5.get(Slider.class, "[Z]").setValue(Ztgt); 
    cp5.get(Slider.class, "Gripper").setValue(angles[6]);
    //cp5.get(Slider.class, "J5").setValue(APtgt[step][6]*9/50);
}

void update_Screen(){// update the screen text infos
  int z1 = 520, z8 = 280;
  fill(0,0,20);
  stroke(90);
  rect(25,25,750,435); // clear all
  rect(25,495,750,145); //debug field
  fill(0,0,40);
  rect(40,40,720,200); //IK upper field
  rect(40,255,720,130); //mid field
  rect(40,400,720,45); // lower
  fill(255);
  //image(img, 620, 100, 74, 85);
  
  text("play: " + playMode ,60, z8);
  text("step: " + (step) + "/" + stepMax ,160, z8); z8 += 14;
  text("record: " + !playMode ,60, z8);
  text("done: " + isAllConverge ,160, z8); z8 += 14;
  
  text("IK: " + inverseKinematic ,60, z8);
  text("pause: " + pause ,160, z8); z8 += 14;
  text("IK key : " + manualIK ,60, z8);
  text("wait: " + (wait -cnt0) ,160, z8); z8 += 14;
  text("IK focus: " + focusC ,60, z8);z8 += 14;
  
  text("gripper: " + gripper ,60, z8);
  text("speed: " + speedMax ,160, z8); z8 += 14;
  text("pump: " + pump ,60, z8); //z8 += 14;
  
  text("math: " + math ,160, z8);
  text("sequence: " + fileName ,260, z8); 
  //text("focus: " + focusC ,420, 220);
  int sx = 420; z8 = 280;
  text("X: " + Xtgt ,sx, z8); z8 += 14;
  text("Y: " + Ytgt ,sx, z8); z8 += 14;
  text("Z: " + Ztgt ,sx, z8); z8 += 14;
  //text("focus: " + focusC ,sx, z8);z8 += 14;
  
  if(DEBUG){
    int xx = 60; z1 = 520;
    text("J0  " + posD[0] ,xx, z1); z1 += 14; // DEBUG data
    text("J1  " + posD[1] ,xx, z1); z1 += 14;
    text("J2  " + posD[2] ,xx, z1); z1 += 14;
    text("J3  " + posD[3] ,xx, z1); z1 += 14;
    text("J4  " + posD[4] ,xx, z1); z1 += 14;
    text("J5  " + posD[5] ,xx, z1); z1 += 14;
    text("J6  " + posD[6] ,xx, z1); z1 += 14;
    //text("sensor data",xx, z1);
    xx = 140; z1 = 520;
    text("err0  " + Perr[0] ,xx, z1); z1 += 14;
    text("err1  " + Perr[1] ,xx, z1); z1 += 14;
    text("err2  " + Perr[2] ,xx, z1); z1 += 14;
    text("err3  " + Perr[3] ,xx, z1); z1 += 14;
    text("err4  " + Perr[4] ,xx, z1); z1 += 14;
    text("err5  " + Perr[5] ,xx, z1); z1 += 14;
    text("err6  " + Perr[6] ,xx, z1); z1 += 14;
    text("errM  " + PerrMax ,xx, z1); z1 += 14;
    xx = 220; z1 = 520;
    text("spd0  " + speed[0] ,xx, z1); z1 += 14;
    text("spd1  " + speed[1] ,xx, z1); z1 += 14;
    text("spd2  " + speed[2] ,xx, z1); z1 += 14;
    text("spd3  " + speed[3] ,xx, z1); z1 += 14;
    text("spd4  " + speed[4] ,xx, z1); z1 += 14;
    text("spd5  " + speed[5] ,xx, z1); z1 += 14;
    text("spd6  " + speed[6] ,xx, z1); z1 += 14;
    xx = 300; z1 = 520;
    text("a0  " + angles[0] ,xx, z1); z1 += 14;
    text("a1  " + angles[1] ,xx, z1); z1 += 14;
    text("a2  " + angles[2] ,xx, z1); z1 += 14;
    text("a3  " + angles[3] ,xx, z1); z1 += 14;
    text("a4  " + angles[4] ,xx, z1); z1 += 14;
    text("a5  " + angles[5] ,xx, z1); z1 += 14;
    text("a6  " + angles[6] ,xx, z1); z1 += 14;

    xx = 380; z1 = 520;
    text("f0  " + force[0] ,xx, z1); z1 += 14;
    text("f1  " + force[1] ,xx, z1); z1 += 14;
    text("f2  " + force[2] ,xx, z1); z1 += 14;
    text("f3  " + force[3] ,xx, z1); z1 += 14;
    text("f4  " + force[4] ,xx, z1); z1 += 14;
    text("f5  " + force[5] ,xx, z1); z1 += 14;
    text("f6  " + force[6] ,xx, z1); z1 += 14;
    xx = 430; z1 = 520;
    text("V56x  " + APtgt[step][10] ,xx, z1); z1 += 14;
    text("V56y  " + APtgt[step][11] ,xx, z1); z1 += 14;
    text("V56z  " + APtgt[step][12] ,xx, z1); z1 += 14;
    text("V67x  " + APtgt[step][13] ,xx, z1); z1 += 14;
    text("V67y  " + APtgt[step][14] ,xx, z1); z1 += 14;
    text("V67z  " + APtgt[step][15] ,xx, z1); z1 += 14;
    text("theta5 " + Z67 ,xx, z1); z1 += 14;
    text("theta6 " + theta6 ,xx, z1); z1 += 14;
  }//end debug

}
