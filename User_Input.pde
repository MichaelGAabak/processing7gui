// read the buttons and keyboard
//
//

// ================================================= get buttons
public void teach(int theValue) {
    inverseKinematic = false;
    manualIK = false;
    KmodeFlag = 1; // manual tech in, point to point 
    go_recmode();
  }
public void play(int theValue) {
  manualIK = false;
  if (stepMax > 0)
    go_playMode();
  }
public void InvK (int theValue) { // needs
  if (manualIK) {manualIK = false; return;}
  if(inverseKinematic){
    inverseKinematic = false;
    KmodeFlag = 1; // forward kinematic, point to point
  }
  else{
    for (int i=0; i < SERVO_NUM; i++){ // set speed for manual moves
      speed [i] = 50;
    }
    setSpeed(fluentEnables, speed); // set speed for manual moves
    inverseKinematic = true;
    //manualIK = false;
    KmodeFlag = 3; // inverse kinematic, point to point
    go_recmode();
    setForceStatus(1);
  }
}
public void add(int theValue) { 
  if (!playMode)
    pos_record();
  }
public void clr(int theValue) {
  if (step > 0)
    pos_clear();
  }
public void pump(int theValue) { // needs
  if(!pump){
    Vpump = 130 ;}
  else {
    Vpump = 430;}
  pump = !pump;
  gripper = false;
}
public void focus(int theValue) { // needs
  focusC = !focusC;
}
public void gripper(int theValue) { // needs
  gripper = !gripper;
}
public void KEY(int theValue) { // back one step
    if (inverseKinematic){
      manualIK = !manualIK;
      println("keyboard IK = " + manualIK);
    }
}
public void math(int theValue) { // back one step
  math = !math;
}
public void back(int theValue) { // back one step
  if (step > 1){
  step -= 1;
  one_step();}
}
public void fwd(int theValue) { // fwd one step
  if (step < stepMax)
  step += 1;
  one_step();
}
public void save(int theValue) {
  selectOutput("Save file (no '.csv' file extension!)", "fileSaveSelected");
}
void fileSaveSelected(File selection) {
  if (selection == null) {
    //
  } 
  else {
    println("Path: " + selection.getAbsolutePath());
    fileName = selection.getName();
    safe_array();}
}
public void load(int theValue) {
    selectInput("Select a sequence to load:", "fileLoadSelected");}
void fileLoadSelected(File selection) {
  if (selection == null) {
    //
  }
  else {
    fileName = selection.getName();
    fileName = fileName.substring(0, (fileName.length()-4));  // Returns "CC"
    read_file();}
}
void controlEvent(ControlEvent theEvent) {
  if(theEvent.isAssignableFrom(Textfield.class)) {
    fileName = theEvent.getStringValue();
    println("your filename input = " + fileName);
  }
  if(theEvent.isAssignableFrom(Button.class)) {
    //
  }
}




// ================================================= get keyboard shortcuts
void read_key(){
  if (keyPressed) { // keyboard command input 
      if (key == ' ' && !playMode) {// record pos 
        delay(200);
        pos_record();
      }
      if (key == 'c' || key == 'C') { // clear pos 
          delay(200);
          if (step > 0)
            pos_clear();
      }
      if (key == 'p' || key == 'P') {// switch to playmode
        delay(200);
        if (stepMax > 0) // only if data availible
          go_playMode();
      }
      if (key == 'k' || key == 'k') {// toggle key input while IK mode
        delay(200);
        if (inverseKinematic){
          manualIK = !manualIK;
          println("keyboard IK: " + manualIK);
        }
      }
      if (key == 's' || key == 'S') {// toggle speed
        delay(200);
        speedMax += 25;
        if (speedMax > 250) speedMax = 50;
      }
      if (key == 'm' || key == 'M') {// toggle speed
        delay(200);
        math = !math;
        println("math: " + math);
      }
      if (key == '1' || key == '2') // // fwd/back one step
      {
        delay(200);
        if (key == '1' && step > 1) step -= 1;
        else if (key == '2' && step < stepMax) step += 1;
        one_step();

      }
  }
}//end keyboard

void one_step(){ // used while cycling through steps
    for (int i=0; i < SERVO_NUM; i++){ // old = newpos
    Pnow[i] = Ptgt[i];}        
    read_Array();
    calculate_speed();
    setServoAngles(angles);  // set pose to arduino
    delay(50); // crapy thing. wait to make sure isAllConverge is false
    playMode = false;
    inverseKinematic = false;
    cnt0 = 0;
}
void read_IKkey(){// keyboard command input for IK linear moves
    if (keyPressed) { 
      if (keyCode == UP){
        delay(200);
        Ytgt -= 10;}
      if (keyCode == DOWN){
        delay(200);
        Ytgt += 10;}
      if (keyCode == LEFT){
        delay(200);
        Xtgt -= 10;}
      if (keyCode == RIGHT){
        delay(200);
        Xtgt += 10;}
      if (key == '.'){
        delay(200);
        Ztgt += 10;}
      if (key == ','){
        delay(200);
        Ztgt -= 10;}
    update_IKslider();
    }
}