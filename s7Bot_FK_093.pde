
// processing for 7Bot Beta version 0.9.3 > Pinaut 5/2016
// sorry for the mess!
//
// ============== READ THIS ===================== !!!!!!
// this code alows you teach the robot by hand or IK, safe positions and replay later.
//
// download the libary for sliders (https://github.com/sojamo/controlp5)
// upload softwareSystem.ino from https://github.com/7Bot/7Bot-Arduino-lib to the arduino before start this sketch!!!
// calculations in the sketch are done with 0-1000 from the servo feedback NOT in degrees
// !servo speed not propper working with slow speeds, because the resolution in the arduino libary is poor (1-25)!
// Use Buttons or Keyboard shortcuts: 
// 'SPACE' record/add a step
// 'C' clear step
// 'P' play the stored positions (restart while pause)
// 'K' enables keyboard move while IK mode
// 'S' change speed (50 to 250)


// ======================= edit here before start
int PORT_ID =  0;// Your should change PORT_ID accoading to your own situation.
int wait = 60; // 60 seconds pause before start play again (pressing 'P' start move manualy while pause) 
int speedMax = 250; // 50-250 maximum servo speed (good above 180!)
boolean DEBUG = true; // extendet data window
// ======================= if you edit below u need to now what u do


import controlP5.*; // libary for sliders (https://github.com/sojamo/controlp5)
ControlP5 cp5;
Slider2D XYslider;
Slider Zslider;
Slider2D V56slider;
Slider V56Zslider;
Slider2D V67slider;
Slider V67Zslider;
Slider Gripper;
Knob V67Zknob;
import processing.serial.*; // start serial communication
Serial myPort; 
int BAUD_RATE = 115200;
int SERVO_NUM = 7;
PImage img;


 
int[] posD = new int[SERVO_NUM]; // servo position array
int[] force = new int[SERVO_NUM]; // servo force array

int step = 0, stepMax = 0, PerrMax, cnt0, cnt1; // flags for data array
int[][] APtgt = new int[500][20]; //[step][ServoPos] arrays to store data (100 steps)
int[] speed = {10, 10, 10, 10, 10, 10, 10}; // array for induvidual servo speed
float[] angles = {90, 130, 65, 90, 90, 90, 75}; // array for induvidual servo angles send to arduino
int[] Pnow = new int[20]; // momentary position 0-7 + 8-13 IK vectors
int[] Ptgt = new int[20]; // target position
int[] Perr = new int[20]; // error/offset between Pnow <> Ptgrposition. needed for speed calculation
int Xtgt = 100, Ytgt = 200, Ztgt = 150; // IK coordinates
int X56 = 0, Y56 = 200, Z56 = -10; // IK vector coordinates for Vec56
int X67 = 200, Y67 = 0, Z67 = 0; // IK vector oordinates fot Vec67
int KmodeFlag = 3, a, w; // flag for forward or inverse kinematic 1KFPP 2FKTR 3IKPP 4IKTR 
int Vpump = 380;
float theta5, theta6; // IK stuff
PVector j6;
PVector vec56 = new PVector(0, 0, -1);
PVector vec67 = new PVector(1, 0, 0);
//PVector[][] Poses = new PVector[5][4];
int  theta;
Table table; // used to safe 2 a file
boolean[] fluentEnables = {true, true, true, true, true, true, true}; // soft acc-dec
boolean isAllConverge = false; // flag move is done
boolean  stepdone = true, pause = false, playMode = false, gripper = false, manualIK = false;// boolean switches
boolean  pump = false, inverseKinematic = false, focusC = false, math = false;// boolean switches
String fileName = "empty"; // give a name for the csv safe/read file
//servo = true,
long millisnow;
long millisold;

// ================================================================== Setup
void setup() 
{ 
  println("Hello 7Bot!");
  frameRate(50);// 20ms cycle time
  if (DEBUG)
    size(800, 660);
  else
    size(800, 490); // generate window
  Set_Screen(); //
  set_slider();
  startup();
  myPort = new Serial(this, Serial.list()[PORT_ID], BAUD_RATE);   
  delay(500); // Delay 4 seconds to wait 7Bot waking up
  setForceStatus(1); // turn servos on
  for (int i=0; i < SERVO_NUM; i++){ // old = newpos
    Pnow[i] = posD[i];
  }
}

// ================================================================== loop (20ms)
void draw()
{
  millisnow = millis();
  if (pause){ //  -------------------------------------------------- counter to pause the robot
    if (millisnow >= millisold + 1000){ 
      millisold = millis();
      cnt0 += 1;//println("DEBUG wait: " + (wait -count) + "s");
      if (cnt0 > wait-1) {pause = false; cnt0 = 0;step = 1;} // end pause   
    }//end millis
  }
    
  if ((!stepdone) && (isAllConverge)){// ------------------------- wait until move end (isAllConverge = true)    
    stepdone = true; cnt0 = 0;
    for (int i=0; i < SERVO_NUM; i++){ // old = newpos
      //Pnow[i] = APtgt[step][i];
      Pnow[i] = posD[i];
    }
      
    //delay(1000); //temporary delay after 1 step is done for debug
  }
    
  if(playMode) { // --------------------------------------------------------- playMode    
    if ((stepdone) && (!pause))
    {
      step += 1;
      if (step > stepMax) { step -=1; pause = true; cnt0 = 0;return;}//array end. restart
      stepdone = false;
      read_Array();
      calculate_speed();
      setServoAngles(angles);  // set pose to arduino
      delay(75); // crapy thing. wait to make sure isAllConverge is false     
    }
  }// end playMode

  if (inverseKinematic){ // ------------------------------------------====== IK field teach in
    if(manualIK)
      read_IKkey(); // keyboard move +-10
    else
      get_position();
    get_pose(); // get position and vector for joint 5,6,7
    
    //while (force[6] > 1) theta6 = blabla    
    if ( vec56.mag() == 100 ){// hand pointing exctly down. using IK5 
      theta5 = (180 - (posD[0]*9/50)) + Z67;
      setIK(j6, vec56, theta5, theta6);}
    else
      setIK(j6, vec56, vec67, theta6); // hand pointing somewhere using IK6
  }
  else
    update_IKslider();
    
  if (math){
    if ((stepdone) && (!inverseKinematic))
    {
      stepdone = false;
      run_math();
      calculate_speed();
      for (int i=0; i < SERVO_NUM; i++){ // old = newpos
          APtgt[step][i] = posD[i];
      }
      
      setIK(j6, vec56, vec67, theta6); // hand pointing somewhere using IK6
      delay(75); // crapy thing. wait to make sure isAllConverge is false
    }
  }
  
  read_key(); // rdisabled if textfield input 
  update_Screen();// update screen
  update_slider();
} // end draw loop

void go_playMode(){ //isAllConverge setup playMode
    step = 0; cnt0 = 0; pause = false;
    playMode = true;
    inverseKinematic = false;
    setForceStatus(1); //servo = true; //enable servo mode 1
    print("playmode = true / "); println(stepMax + " steps");
    delay(1000);
}

void go_recmode(){ // setup recmode
    playMode = false;
    pause = false; cnt0 = 0;
    //step = stepMax;
    //step = 1;
    print("Record mode... "); println(step);
    if (!inverseKinematic)
      setForceStatus(0); //servo = false; //disable servo
}

 
/*void mouseClicked() {
  //playMode = !playMode;
  println("playMode = " + playMode);
}*/