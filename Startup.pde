// setup stuff
//

void startup(){
  
  table = new Table(); // create table  
  table.addColumn("step"); table.addColumn("function"); table.addColumn("axis0");
  table.addColumn("axis1"); table.addColumn("axis2"); table.addColumn("axis3");
  table.addColumn("axis4"); table.addColumn("axis5"); table.addColumn("axis6");
  table.addColumn("Xtgt"); table.addColumn("Ytgt"); table.addColumn("Ztgt");
  table.addColumn("56x"); table.addColumn("56y"); table.addColumn("56z");// vector56
  table.addColumn("67x"); table.addColumn("67y"); table.addColumn("67z");
  table.addColumn("the");
  println("Read the manual or have fun with try and error.. ;-)");
}

void set_slider(){
  cp5 = new ControlP5(this); // generate  servo sliders
  int xx = 510;
  cp5.addSlider("Joint 0").setPosition(xx,270).setSize(200,10).setRange(0,180);
  cp5.addSlider("Joint 1").setPosition(xx,285).setSize(200,10).setRange(0,180);
  cp5.addSlider("Joint 2").setPosition(xx,300).setSize(200,10).setRange(0,180);
  cp5.addSlider("Joint 3").setPosition(xx,315).setSize(200,10).setRange(0,180);
  cp5.addSlider("Joint 4").setPosition(xx,330).setSize(200,10).setRange(0,180);
  cp5.addSlider("Joint 5").setPosition(xx,345).setSize(200,10).setRange(0,180);
  cp5.addSlider("Joint 6").setPosition(xx,360).setSize(200,10).setRange(0,180);
  //buttons
  int yy = 415;
  cp5.addButton("teach").setPosition(60,yy).setSize(40,16);
  cp5.addButton("InvK").setPosition(110,yy).setSize(40,16);
  cp5.addButton("KEY").setPosition(160,yy).setSize(40,16);
  cp5.addButton("focus").setPosition(210,yy).setSize(40,16);
  cp5.addButton("gripper").setPosition(260,yy).setSize(40,16);
  cp5.addButton("pump").setPosition(310,yy).setSize(40,16);
  cp5.addButton("math").setPosition(360,yy).setSize(40,16);
  cp5.addButton("play").setPosition(430,yy).setSize(40,16);
  cp5.addButton("add").setPosition(480,yy).setSize(30,16);
  cp5.addButton("clr").setPosition(520,yy).setSize(30,16);
  cp5.addButton("back").setPosition(560,yy).setSize(30,16);
  cp5.addButton("fwd").setPosition(600,yy).setSize(30,16);  
  cp5.addButton("load").setPosition(650,yy).setSize(40,16);
  cp5.addButton("save").setPosition(700,yy).setSize(40,16);
  //cp5.addTextfield(" ").setPosition(560,yy).setSize(80,16).setFocus(false).setColor(color(255));// name input
  // IK sliders
  XYslider = cp5.addSlider2D("Arm [XY]").setPosition(60,60).setSize(300,150) .setMinMax(-310,10,310,340).setValue(Xtgt,Ytgt);
  Zslider = cp5.addSlider("[Z]").setPosition(370,60).setSize(10,150).setRange(-50,200).setValue(Ztgt);
  V56slider = cp5.addSlider2D("Hand [XY]").setPosition(420,95).setSize(140,115).setMinMax(-300,0,300,500).setValue(0,0);
  V56Zslider = cp5.addSlider(" [Z] ").setPosition(570,95).setSize(10,115).setRange(-100,300).setValue(-100);
  //V67slider = cp5.addSlider2D("Vector67").setPosition(585,60).setSize(100,80).setMinMax(-300,0,300,400).setValue(200,0);
  V67Zknob = cp5.addKnob("J5 ROTATION").setPosition(620,130).setRange(-260,260).setValue(0).setRadius(40).setDragDirection(Knob.HORIZONTAL);
  //V67Zslider = cp5.addSlider("ROTATION").setPosition(420,160).setSize(100,10).setRange(-180,180).setValue(0);
  Gripper = cp5.addSlider("Gripper").setPosition(420,60).setSize(125,10).setRange(20,80).setValue(75);
  cp5.addSlider("Pump").setPosition(420,75).setSize(125,10).setRange(0,1).setValue(0);
}
